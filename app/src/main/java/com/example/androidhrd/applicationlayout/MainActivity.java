package com.example.androidhrd.applicationlayout;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    final int REQUEST_REGISTER=1;


    EditText etName,etPhone,etClass;
    Button btnRegister,btnCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etName=findViewById(R.id.et_name);
        etPhone=findViewById(R.id.et_pnumber);
        etClass=findViewById(R.id.et_classname);
        btnRegister=findViewById(R.id.btn_Register);
        btnCall=findViewById(R.id.btn_Call);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRegisterActivity();
            }
        });

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber=etPhone.getText().toString();
                if(!phoneNumber.isEmpty()){
                    Toast.makeText(MainActivity.this,"Calling : "+phoneNumber,Toast.LENGTH_LONG).show();
                    Intent intent=new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+phoneNumber));
                    startActivity(intent);
                }else{
                    Toast.makeText(MainActivity.this,"Please Input Phone Number",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_REGISTER && resultCode==1){
            etClass.setText("");
            etName.setText("");
            etPhone.setText("");
            etName.requestFocus();
        }
    }

    public void startRegisterActivity(){
        try{
            int phoneNumber=Integer.valueOf(etPhone.getText().toString());

            Intent intent=new Intent(this,RegisterActivity.class);
            Student student=new Student(etName.getText().toString(),phoneNumber,etClass.getText().toString());
            intent.putExtra("mystudent",student);
            startActivityForResult(intent,REQUEST_REGISTER);
        }catch (NumberFormatException e){
            Toast.makeText(MainActivity.this,"Please Input Data Correctly",Toast.LENGTH_LONG).show();

        }

    }



}
